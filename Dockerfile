FROM golang:alpine

COPY . /project

WORKDIR /project

RUN apk add make && apk add git && make build

CMD ["/project/bin/app"]
